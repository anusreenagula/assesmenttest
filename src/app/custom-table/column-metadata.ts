export interface ColumnMetaData {
  headerText: string;
  propertyName: string;
}
