import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ColumnMetaData } from './column-metadata';

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.css']
})
export class CustomTableComponent implements OnInit {
  @Input() data: any[];
  @Input() pageSize: number;
  @Input() columns: ColumnMetaData[];
  @Output() submit = new EventEmitter<any>();
  pageIndex = 0;

  constructor() {}

  ngOnInit() {}

  getDisplayData() {
    const start = this.pageIndex * this.pageSize;
    return this.data.slice(start, start + this.pageSize);
  }

  onNextClick() {
    if (this.pageIndex < this.data.length / this.pageSize) {
      this.pageIndex++;
    }
  }
  onSubmit(d) {
    this.submit.emit(d);
  }

  onPrevClick() {
    if (this.pageIndex > 1) {
      this.pageIndex--;
    }
  }

  onChange(event: { target: { value: number } }) {
    this.pageSize = event.target.value;
    this.pageIndex = 0;
  }
}
