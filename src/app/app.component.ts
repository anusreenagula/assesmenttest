import { Component } from '@angular/core';

import { sampleData } from './sample-data';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'capco-assement';

  constructor(private http: HttpClient) {}

  data = sampleData;
  columns = [
    { headerText: 'Name', propertyName: 'name' },
    { headerText: 'Phone', propertyName: 'phone' },
    { headerText: 'Email', propertyName: 'email' },
    { headerText: 'Company', propertyName: 'company' },
    { headerText: 'Date Of Entry', propertyName: 'date_entry' },
    { headerText: 'Org Num', propertyName: 'org_num' },
    { headerText: 'Address 1', propertyName: 'address_1' },
    { headerText: 'City', propertyName: 'city' },
    { headerText: 'Zip', propertyName: 'zip' },
    { headerText: 'Geo', propertyName: 'geo' },
    { headerText: 'Pan', propertyName: 'pan' },
    { headerText: 'Pin', propertyName: 'pin' },
    { headerText: 'Status', propertyName: 'status' },
    { headerText: 'Fee', propertyName: 'fee' },
    { headerText: 'Date Exit', propertyName: 'date_exit' },
    { headerText: 'Date First', propertyName: 'date_first' },
    { headerText: 'Date Recent', propertyName: 'date_recent' }
  ];
  onSubmit(data) {
    this.http.post('/api/submit', data).subscribe(console.log);
  }
}
